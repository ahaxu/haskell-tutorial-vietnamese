## Repo này đã migrated qua gitlab
Gitlab: https://gitlab.com/ahaxu/haskell-tutorial-vietnamese

#### Học haskell Tiếng Việt

Chào các bạn đến vơí kênh lập trình ahaxu

Link youtube https://www.youtube.com/user/logauit

### Thời gian dạy online 2023

- Bắt đầu: 1st Just 2023
- Thời gian 8:30-9:15pm, Sat

Một buổi 30-45 phút (bao gồm QA)

#### Thư mục overview

Lý thuyết cơ bản theo trình tự:

- Giới thiệu về haskell
    - Khởi tạo môi trường học haskell nhanh gọn
    - Reverse list, merge sort
    - Basic syntax
    - Condition
- Data type, data declaration
    - Pattern matching
- Haskell fold left và fold right
- Bài tập về list trong haskell
- Function composition
- Haskell type class
- Foldable, Monoid, Semigroup type class
- Traversable type class
- Sơ lược về category và functor
- Reader functor
- Applicative
- Monad
- So sánh functor, applicative, monad
- IO monad
- IO monad va bai tap xu ly file
- State data type
- Monad transformer

#### Thư mục system-f-course

Ghi chú liên quan đến bài tập từ [repo](https://github.com/system-f/fp-course)

#### Để build file slide từ markdown file

```
# ví dụ chúng ta muốn convert bài 9 từ file md
pandoc -s --webtex -i -t slidy ./overview/9_monad_transformation.md -o ./overview/9_monad_transformation.html

```

