{-# LANGUAGE FlexibleInstances #-}

import           Control.Monad

{-
-- TODO try with Compose' f (Optional a)
-- TODO implement IOT IOT' (check 9_monad_trans.hs)
-- conclusion: can't generlize f g but for specific f g is okay for eg: MaybeT, EitherT, ...

-- p1: in some case we want to mix/stack/combine/compose monad (not composition)
-- p1.1: try compose.hs
-- p1.2: why we can't generalize
-- p1.3: try with Compose' (f (Optional a)) vs Compose'(Optional ( g a))
-- p1.4: try with Compose' (f (IO a)) vs Compose'(IO ( g a))

-- p2: refactoring boiler plate with monad transformer
--
-- State exercise
-- StateT exercise
-}
newtype IOT m a =
  IOT
    { runIOT :: m (IO a)
    }

newtype IOT' m a =
  IOT'
    { runIOT' :: IO (m a) -- if m is Maybe we can use MaybeT, same as other monad trans
    }

instance Functor (IOT' Maybe) where
  fmap = undefined

instance Applicative (IOT' Maybe) where
  pure = undefined
  k <*> fa = undefined

instance Monad (IOT' Maybe) where
  return = IOT' . return . Just
  x >>= k =
    IOT' $
    let IOT' ioma = x -- pattern matching
     in ioma >>= \ma ->
          case ma of
            Nothing -> return Nothing
            Just a  -> runIOT' $ k a

instance Functor (IOT Maybe) where
  fmap = undefined

instance Applicative (IOT Maybe) where
  pure = undefined
  k <*> fa = undefined

instance Monad (IOT Maybe) where
  return x = IOT (Just (return x))
  IOT Nothing >>= _ = IOT Nothing
    --IOT (Just a) >>= k = _todo
  IOT (Just ioa) >>= k -- k      :: a -> IOT (Maybe b)
                                                -- ioa    :: IO a
   =
    let foo a = runIOT $ k a -- foo    :: a -> Maybe (IO b)
        iomiob = fmap (\a -> foo a) ioa -- iomiob :: IO (Maybe (IO b))
     in IOT $ error "how can we convert `IO a -> a`" -- _todo  :: Maybe (IO b)
