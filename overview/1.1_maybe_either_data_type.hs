module MaybeEitherDataTypeExample where

safeDiv
    :: Integral a
    => a
    -> a
    -> Maybe a
safeDiv a b
    | b == 0     = Nothing
    | otherwise = Just $ a `div` b


safeDiv'
    :: Integral a
    => a
    -> a
    -> Either String a
safeDiv' a b
    | b == 0     = Left "can't divide by zero"
    | otherwise = Right $ a `div` b

