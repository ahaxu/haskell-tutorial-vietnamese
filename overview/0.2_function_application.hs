
module FuncApplicationExample where

-- một cách viết khác của hàm add
addInteger :: Integer -> Integer -> Integer
addInteger = (+)

-- một cách viết khác của hàm even
isEven :: Integer -> Bool
isEven = even

-- combine 2 hàm lại với nhau
combineFunc :: Integer -> Integer -> Bool
combineFunc a b = isEven (addInteger a b) -- (1)


combineFunc' :: Integer -> Integer -> Bool
combineFunc' a b = isEven $ addInteger a b

combineFunc'' :: Integer -> Integer -> Bool
combineFunc'' = (isEven .) . addInteger -- g (f x) = (g . f) x

