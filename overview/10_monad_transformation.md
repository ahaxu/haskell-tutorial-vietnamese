% 10. Monad transformers 
% lk
% 2022


1. Làm bài tập liên quan `Compose.hs`

  - Không thể implement `(>>=)` cho Compose (f (g a))

  - Thử tổng quát hoá f hoặc g

    - Compose' (f (Optional a)) vs Compose2 (Optional ( g a))


2. Xét 1 ví dụ về mix/compose nhiều monad lại với nhau

    - Có cách nào để tránh boilerplate code như trên không?

    - Giới thiệu MaybeT/ EitherT
        - link liên quan: https://github.com/ahaxu/monad_transformer_tut

    - Tai sao lại ko có IOT (IO transformer) (Todo ??)


3. Tài liệu tham khảo

- [Bài tập về compose](https://github.com/system-f/fp-course/blob/master/src/Course/Compose.hs)
- [Adam McCullough - Monad Transformers for the Easily Confused - λC 2018
](https://youtu.be/SMj-n2f7wYY) 

