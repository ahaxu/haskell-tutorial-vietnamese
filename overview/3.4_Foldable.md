% 3.4 Foldable
% lk@AhaXu
% 2022

# 3.4 Foldable

```
~> import Data.Foldable
~> :t fold
fold
  ::(Foldable t, Monoid m)
  => t m
  -> m

~> :info Foldable
class Foldable t where
  fold :: Monoid m => t m -> m
  foldMap :: Monoid m => (a -> m) -> t a -> m
                     -- b1 = map f <$> t a :: t m
                     -- b2 = fold b1 :: t m -> m
  ...
  {-# MINIMAL foldMap | foldr #-}


~> :i Monoid
class Semigroup a => Monoid a where
  mempty :: a
  mappend :: a -> a -> a
  mconcat :: [a] -> a
  {-# MINIMAL mempty #-}

~> :i Semigroup
class Semigroup a where
  (<>) :: a -> a -> a
  ...
  {-# MINIMAL (<>) #-}
```

Các bài tập liên quan Foldable

