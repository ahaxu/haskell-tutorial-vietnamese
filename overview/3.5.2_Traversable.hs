{-# LANGUAGE InstanceSigs #-}

module TraversableSample where

import Data.Foldable
import qualified Data.Map as M
import Data.Monoid
import Prelude hiding (id)


--
-- traverse
--  :: (Traversable t, Applicative f)
--  => (a -> f b) -> t a -> f t b
--

--
-- Traversable with Student data type
--
type StudentID = Integer

data Student = Student
    { id :: StudentID
    , firstName :: String
    , lastName :: String
    }
    deriving (Show)

students :: [(StudentID, Student)]
students =
    let
        xs = (\x ->
                let
                    std = Student
                        { id = x
                        , firstName = "firstName " <> show x
                        , lastName = "lastName " <> show x
                        }
                in (x, std)
            ) <$> [1..5]
    in xs

findStudent :: StudentID -> Maybe Student
findStudent stdID =
    let m = M.fromList students
    in M.lookup stdID m

-- student example with break the loop when any student id not found
-- when we want to break within loop immediately
findStudents :: [StudentID] -> Maybe [Student]
findStudents = traverse findStudent

findStudent' :: StudentID -> IO (Maybe Student)
findStudent' stdID =
    let
        m = M.fromList students
        rs = M.lookup stdID m
    in pure rs

-- student example without break loop when student id not found
-- when we dont wanna break loop when nothing return
findStudents' :: [StudentID] -> IO [Maybe Student]
findStudents' = traverse findStudent'

main :: IO ()
main = do
    putStrLn ">> student example with break the loop when any student not found"

    putStrLn "find student with id = 1"
    print $ findStudent 1

    putStrLn "find student with id 1..5"
    print $ findStudents [1 .. 5]

    putStrLn "find student with id 1..6, expected Nothing"
    print $ findStudents [1 .. 6]

    putStrLn "========================================="

    putStrLn ">> student example without break the loop"

    putStrLn "find student with id = 1"
    std <- findStudent' 1
    print std

    putStrLn "find student with id 1..5"
    stds <- findStudents' [1 .. 5]
    print stds

    putStrLn "find student with id 1..6"
    stds' <- findStudents' [1 .. 6]
    print stds'

