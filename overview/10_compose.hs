{-# LANGUAGE InstanceSigs        #-}
{-# LANGUAGE NoImplicitPrelude   #-}
{-# LANGUAGE ScopedTypeVariables #-}

--
-- extract from https://gitlab.com/nguyentienlong1/learn-haskell/-/blob/proof-that-we-cant-generalize-f-g/src/Course/Compose.hs
--
module Course.Compose where

import           Course.Applicative
import           Course.Contravariant
import           Course.Core
import           Course.Functor
import           Course.Monad
import           Course.Optional

-- Exactly one of these exercises will not be possible to achieve. Determine which.
newtype Compose f g a =
  Compose (f (g a))
  deriving (Show, Eq)

-- Implement a Functor instance for Compose
instance (Functor f, Functor g) => Functor (Compose f g) where
  (<$>) :: (a -> b) -> Compose f g a -> Compose f g b
  (<$>) k (Compose fga) = Compose $ (k <$>) <$> fga

instance (Applicative f, Applicative g) => Applicative (Compose f g) where
  pure = Compose . pure . pure
  (<*>) :: Compose f g (a -> b) -> Compose f g a -> Compose f g b
  (<*>) (Compose fgk) (Compose fga) = Compose $ lift2 (<*>) fgk fga

-- Implement the pure function for an Applicative instance for Compose
-- Implement the (<*>) function for an Applicative instance for Compose
instance (Monad f, Monad g) => Monad (Compose f g) where
  (=<<) :: (a -> Compose f g b) -> Compose f g a -> Compose f g b
  (=<<) k (Compose fga) =
    let x = (\ga -> k <$> ga) <$> fga -- f (g (Compose f g b))
     in undefined

-- Implement the (=<<) function for a Monad instance for Compose
-- Monad transformer part 2
-- Compose' (f (Optional a)) vs Compose' (Optional ( g a))
--data Compose' f a = Compose' { getCompose' :: f (Optional a)}
data Compose' f a =
  Compose' (f (Optional a)) -- OptionalT f a

instance Functor f => Functor (Compose' f) where
  (<$>) ::
       (a -> b)
    -> Compose' f a -- Compose ' f ( Optional a)
    -> Compose' f b -- Compose ' f ( Optional b)
  (<$>) k (Compose' foa) =
    Compose' $
    let fob =
          (\oa ->
             case oa of
               Empty  -> Empty
               Full a -> Full $ k a) <$>
          foa
     in fob

instance (Applicative f) => Applicative (Compose' f) where
  pure = Compose' . pure . Full
  (<*>) ::
       Compose' f (a -> b) -- Compose' f Optional (a -> b)
    -> Compose' f a -- Compose' f Optional a
    -> Compose' f b -- Compose' f Optional b
  (<*>) (Compose' foab) (Compose' foa) = Compose' $ lift2 (<*>) foab foa

instance (Monad f) => Monad (Compose' f) where
  (=<<) :: (a -> Compose' f b) -> Compose' f a -> Compose' f b
  (=<<) k (Compose' foa) =
    let fob =
          foa >>= \oa ->
            case oa of
              Empty -> pure Empty
              Full a ->
                case k a of
                  Compose' fob -> fob
     in Compose' fob

-- Compose2 (Optional ( g a))
data Compose2 g a =
  Compose2 (Optional (g a))

instance Functor g => Functor (Compose2 g) where
  (<$>) :: (a -> b) -> Compose2 g a -> Compose2 g b
  (<$>) k (Compose2 oga) =
    Compose2 $
    case oga of
      Empty   -> Empty
      Full ga -> Full $ k <$> ga

instance (Applicative g) => Applicative (Compose2 g) where
  pure = Compose2 . Full . pure
  (<*>) :: Compose2 g (a -> b) -> Compose2 g a -> Compose2 g b
  (<*>) (Compose2 ogab) (Compose2 oga) = Compose2 $ lift2 (<*>) ogab oga

instance (Monad g) => Monad (Compose2 g) where
  (=<<) :: (a -> Compose2 g b) -> Compose2 g a -> Compose2 g b
  (=<<) k (Compose2 oga) =
    let ogb =
          case oga of
            Empty -> Empty
            Full ga ->
              Full $
              ga >>= \a ->
                let y = k a
                 in case y of
                      Compose2 ogb' ->
                        case ogb' of
                          Empty   -> undefined
                         -- ^ need to construct data with type g b
                          Full gb -> gb
     in Compose2 ogb
