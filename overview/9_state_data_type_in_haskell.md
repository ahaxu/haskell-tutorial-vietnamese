% 9. `State` data type trong haskell và bài tập liên quan
% lk
% 2022

## `State` data type trong haskell và bài tập liên quan

### Link video

- Phần 1: https://youtu.be/-smAnOpP9QU

- Phần 2: (coming soon) 

### Link bài tập

[fp-course từ system-f](https://github.com/system-f/fp-course/blob/master/src/Course/State.hs)

### Link tham khảo

- https://wiki.haskell.org/State_Monad (bài tập và giải thích khá hay)

