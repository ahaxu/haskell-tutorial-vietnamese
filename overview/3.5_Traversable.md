% 3.5 Traversable
% lk@AhaXu
% 2022

# 3.5 Traversable

```
class (Functor t, Foldable t) => Traversable t where
  traverse :: Applicative f => (a -> f b) -> t a -> f (t b)
               -- k <$> t a :: (a -> f b) -> t a -> t (f b)
  sequenceA :: Applicative f => t (f a) -> f (t a)
  mapM :: Monad m => (a -> m b) -> t a -> m (t b)
  sequence :: Monad m => t (m a) -> m (t a)
  {-# MINIMAL traverse | sequenceA #-}

```

Ví dụ về traversable

```
traverse print [1..5]
traverse_ print [1..5]
```

Bài tập về Traversable

- Bài tập fp-course
- Bài tập về Binary Tree 
```
data Tree a = Empty | Branch a (Tree a) (Tree a)
              deriving (Show, Eq)
```
  - in các branch
  - in các branch là số lẻ 
