{-# LANGUAGE InstanceSigs #-}


module TraversableSample where

import Data.Foldable
import qualified Data.Map as M
import Data.Monoid

--
-- Traversable example with Tree data type
--
data Tree a
    = Empty
    | Node a (Tree a) (Tree a)
    deriving (Show)

instance Functor Tree where
    fmap :: (a -> b) -> Tree a -> Tree b
    fmap f Empty = Empty
    fmap f (Node a l r) =
        let
            b = f a
            l' = fmap f l
            r' = fmap f r
        in Node b l' r'

instance (Semigroup a) => Semigroup (Tree a) where
    (<>)
        :: Tree a
        -> Tree a
        -> Tree a
    (<>) t1 Empty = t1
    (<>) Empty t1 = t1
    (<>) (Node a l1 r1) (Node b l2 r2) =
        let
            l = l1 <> l2
            r = r1 <> r2
        in Node (a<>b) l r


instance Monoid a => Monoid (Tree a) where
    mempty = Empty
    mappend = (<>)

instance Foldable Tree where
    foldMap
        :: (Monoid m)
        => (a -> m)
        -> Tree a
        -> m
    foldMap f Empty = mempty
    foldMap f (Node a l r) =
        let
            m = f a
            l' = foldMap f l
            r' = foldMap f r
        in m <> l' <> r'

instance Traversable Tree where
    traverse ::
        Applicative f =>
        (a -> f b) ->
        Tree a ->
        f (Tree b)
    traverse k Empty = pure Empty
    traverse k (Node a l r) =
        let
            fb = k a           -- :: f b
            fl = traverse k l  -- :: f (Tree b)
            fr = traverse k r  -- :: f (Tree b)
        in Node <$> fb <*> fl <*> fr --- :: f (Tree b)

printTree
    :: Show a
    => Tree a
    -> IO ()
printTree Empty = pure ()
printTree (Node a l r) = do
    print a
    printTree l
    printTree r


main :: IO ()
main = do
    let l1 = Node 1 Empty Empty
        l2 = Node 2 Empty Empty
        l3 = Node 3 Empty Empty
        l4 = Node 4 l1 l2
        t = Node 5 l4 l3

    print t

    print "sum and fmap ======"
    print $ Sum <$> t

    print "fold and sum ======"

    print $ fold $ Sum <$> t

    print "foldMap ======"
    print $ foldMap Sum t

    print "print with printTree ====="
    printTree t

    print "traverse_ ====="
    traverse_ print t

    print "traverse ====="
    traverse print t >> pure ()

