% 7. Applicative
% lk
% 2022

### Applicative in haskell

#### Định nghĩa Applicative 

```
class Functor f => Applicative f where
    pure :: a -> f a
    (<*>) :: f (a -> b) -> f a -> f b
```

Chúng ta gọi <*> là "ap" 

- Ví dụ

#### So sánh giữa fmap và <*> 
```
(<$>) :: Functor f =>       (a -> b) -> f a -> f b
(<*>) :: Applicative f => f (a -> b) -> f a -> f b
```

#### Applicative law

- identity `pure id <*> v = v`

- homomorphism `pure f <*> pure x = pure (f x)`

- composition `pure (.) <*> u <*> v <*> w = u <*> (v <*> w)`

- interchangble `u <*> pure y = pure ($ y) <*> u`

- recap fmap
    - identity `fmap id == id`
    - preserver structure `fmap :: Functor f => (a->b) -> f a -> f b`
    - composition `fmap (f . g) = fmap f . fmap g`

- Quick check

#### Bài tập liên quan

https://github.com/system-f/fp-course/blob/master/src/Course/Applicative.hs

### Tài liệu tham khảo 

- Book: https://www.goodreads.com/en/book/show/25587599-haskell-programming-from-first-principles
