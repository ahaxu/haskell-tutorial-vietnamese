module XMonad where

import           Control.Applicative (Applicative (liftA2))

---------------
-- maybePlus --
---------------
maybePlus :: Maybe Int -> Maybe Int -> Maybe Int
maybePlus ma mb =
  case ma of
    Nothing -> Nothing
    Just a ->
      case mb of
        Nothing -> Nothing
        Just b  -> Just (a + b)

-- helper function
andThen :: Maybe a -> (a -> Maybe b) -> Maybe b
andThen ma f =
  case ma of
    Nothing -> Nothing
    Just a  -> f a

-- using helper function to refactor
maybePlus' :: Maybe Int -> Maybe Int -> Maybe Int
maybePlus' ma mb = ma `andThen` \a -> mb `andThen` \b -> Just (a + b)

-- Actually we can sipmly to like this
--pure (+1) <*> Just 1 <*> Just 2
maybePlus'' :: Maybe Int -> Maybe Int -> Maybe Int
maybePlus'' ma mb = pure (+) <*> ma <*> mb

----------------
-- eitherPlus --
----------------
type Err = String

eitherPlus :: Either Err Int -> Either Err Int -> Either Err Int
eitherPlus ea eb =
  case ea of
    Left err -> Left err
    Right a ->
      case eb of
        Left err -> Left err
        Right b  -> Right (a + b)

-- helper function
andThen' :: (Either e a) -> (a -> Either e b) -> (Either e b)
andThen' ea f =
  case ea of
    Left e  -> Left e
    Right a -> f a

-- using helper function to refactor
eitherPlus' :: Either Err Int -> Either Err Int -> Either Err Int
eitherPlus' ea eb = ea `andThen'` \a -> eb `andThen'` \b -> Right (a + b)

-- actually we can simply do like this
eitherPlus'' :: Either Err Int -> Either Err Int -> Either Err Int
eitherPlus'' ea eb = pure (+) <*> ea <*> eb

----------------
-- listPlus ----
----------------
listPlus :: [Int] -> [Int] -> [Int]
listPlus as bs =
  case as of
    [] -> []
    a:aas ->
      case bs of
        []  -> []
        bbs -> map (\b -> a + b) bbs ++ (listPlus aas bbs)

-- helper function
andThen'' :: [Int] -> (Int -> [Int]) -> [Int]
andThen'' as f =
  case as of
    []    -> []
    a:aas -> f a ++ andThen'' aas f

-- refactor using helper function
listPlus' :: [Int] -> [Int] -> [Int]
listPlus' as bs = as `andThen''` \a -> bs `andThen''` \b -> [a + b]

-- all in one solution
mPlus :: (Monad m, Num a) => m a -> m a -> m a
mPlus ma mb = ma >>= \a -> mb >>= \b -> return $ a + b

mPlus1 :: (Monad m, Num a) => m a -> m a -> m a
mPlus1 ma mb = do
  a <- ma
  b <- mb
  return $ a + b

apPlus :: (Applicative f, Num a) => f a -> f a -> f a
apPlus fa fb = liftA2 (+) fa fb

-- monad composition / kleisli composition
infixr 1 >=>

(>=>) :: Monad m => (a -> m b) -> (b -> m c) -> (a -> m c)
f >=> g = \a -> f a >>= g
